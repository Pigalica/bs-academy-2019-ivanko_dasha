const assert = require('assert');
const {URL} = require('url');

describe('Automation_Testing_Basics_#1', () => {
    
    it('1_Should create new user at Hedonist with valid data', () => {
        browser.url('https://staging.bsa-hedonist.online/signup');

        const firstnameField = $('input[name=firstName]');
        const lastnameField = $('input[name=lastName]');
        const emailField = $('input[name=email]');
        const newpasswordField = $('input[type=password]');
        const createButton = $('button.button.is-primary');
        const message = $('div.toast.is-top');

        firstnameField.setValue('Test');
        lastnameField.setValue('Test');
        emailField.setValue('batodaga-5672@yopmail.com');
        newpasswordField.setValue('test1234');
        createButton.click();

        message.waitForDisplayed(5000);
        const messageText = message.getText();

        assert.equal(messageText, "You have successfully registered! Now you need to login");

    });

    it('2_Should not login to Hedonist with invalid credentials', () => {
        browser.url('https://staging.bsa-hedonist.online');

        const emailField = $('input[name=email]');
        const passField = $('input[type=password]');
        const loginButton = $('button.button.is-primary');
        const errormessage = $('div.toast.is-top');
        
        emailField.setValue('ivanko.dasha@@gmail.com');
        passField.setValue('test1234');
        loginButton.click();

        errormessage.waitForDisplayed(5000);
        const errormessageText = errormessage.getText();

        assert.equal(errormessageText, "The email or password is incorrect");
        
    });

    xit('3_Should create new place at Hedonist with valid data', () => {
        browser.url('https://staging.bsa-hedonist.online');

        const emailField = $('input[name=email]');
        const passField = $('input[type=password]');
        const loginButton = $('button.button.is-primary');
        const dropDown = $('div.profile.navbar-link');
        const link = $('a[href="/places/add"]');
        const nameFieled = $('input.input.is-medium');
        const cityField = $('.tab-content input[placeholder="Location"]');
        const zipField = $('input[placeholder="09678"]');
        const addressField = $('input[placeholder="Khreschatyk St., 14"]');
        const phoneField = $('input[type=tel]');
        const websiteField = $('input[placeholder="http://the-best-place.com/"]');
        const descriptionField = $('.textarea');
        const loginGeneralNextbutton = $('span.button.is-success');
        // Картинку добавить не могу

        emailField.setValue('ivanko.dasha@gmail.com');
        passField.setValue('test123');
        loginButton.click();
        browser.pause(1000);
        dropDown.click();
        link.click();
        browser.pause(1000);
        nameFieled.setValue('My_new_place');
        cityField.setValue(null);
        cityField.setValue('Dnipro');
        zipField.setValue('49000');
        addressField.setValue('Slobozhanskiy avenue');
        phoneField.setValue('380990000000');
        websiteField.setValue('https://www.google.com');
        descriptionField.setValue('Description of new place');
        loginGeneralNextbutton.click();
        // Картинку добавить не могу
       
      });

    it('4_Should create new list at Hedonist with valid data', () => {
        browser.url('https://staging.bsa-hedonist.online');

        const emailField = $('input[name=email]');
        const passField = $('input[type=password]');
        const loginButton = $('button.button.is-primary');
        const dropDown = $('div.profile.navbar-link');
        const link = $('a[href="/my-lists"]');
        const addlistbutton = $('a.button.is-success');
        const listnameField = $('#list-name');
        const savebutton = $('button.button.is-success');
        const alert = $('div.toast.is-top');
                       
        emailField.setValue('ivanko.dasha@gmail.com');
        passField.setValue('test123');
        loginButton.click();
        browser.pause(1000);
        dropDown.click();
        link.click();
        browser.pause(1000);
        addlistbutton.click();
        browser.pause(1000);
        listnameField.setValue('My_New_List');
        savebutton.click();
              
        alert.waitForDisplayed(5000);
        const alertText = alert.getText();

        assert.equal(alertText, "The list was saved!");
                    
    });

    it('5_Should be possibility to delete list at Hedonist', () => {
        browser.url('https://staging.bsa-hedonist.online');

        const emailField = $('input[name=email]');
        const passField = $('input[type=password]');
        const loginButton = $('button.button.is-primary');
        const dropDown = $('div.profile.navbar-link');
        const link = $('a[href="/my-lists"]');
        const addlistbutton = $('a.button.is-success');
        const listnameField = $('#list-name');
        const savebutton = $('button.button.is-success');
        const deletebutton = $('button.button.is-danger:enabled');
        const reconfirmbutton = $('button.button.is-danger:enabled');
        const reply = $('div.toast.is-top');
                            
        emailField.setValue('ivanko.dasha@gmail.com');
        passField.setValue('test123');
        loginButton.click();
        browser.pause(1000);
        dropDown.click();
        link.click();
        browser.pause(1000);
        addlistbutton.click();
        browser.pause(1000);
        listnameField.setValue('My_New_List');
        savebutton.click();
        browser.pause(1000);
        deletebutton.click()
        browser.pause(5000);
        reconfirmbutton.click();
              
        reply.waitForDisplayed(5000);
        const replyText = reply.getText();

        assert.equal(replyText, "The list was removed");                
    });
    
});