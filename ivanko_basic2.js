const assert = require('assert');
const {URL} = require('url');
const credentials = require('./myTestData.json');

describe('Automation_Testing_Basics_#1', () => {
    
    it('1_Should create new user at Hedonist with valid data', () => {
        browser.url('https://staging.bsa-hedonist.online');

        const creatnewAccountbutton = $('a.link.link-signup');
        const firstnameField = $('input[name=firstName]');
        const lastnameField = $('input[name=lastName]');
        const emailField = $('input[name=email]');
        const newpasswordField = $('input[type=password]');
        const createButton = $('button.button.is-primary');
                
        creatnewAccountbutton.click();
        firstnameField.setValue('Test');
        lastnameField.setValue('Test');
<<<<<<< HEAD
        emailField.setValue('icessume-7107@yopmail.com');
=======
        emailField.setValue('apepurrem-9361@yopmail.com');
>>>>>>> cb6f9025d506b16b20bc853dd00bfc73785da7e3
        newpasswordField.setValue('test1234');
        createButton.click();

        const message = $('div.toast.is-top');

        message.waitForDisplayed(5000);
        const messageText = message.getText();
        const actualUrl = new URL(browser.getUrl());
        
        assert.equal(messageText, "You have successfully registered! Now you need to login");
        assert.equal(actualUrl, "https://staging.bsa-hedonist.online/login");
        
<<<<<<< HEAD
=======
        const registeredemailField = $('input[name=email]');
        const registeredpassField = $('input[type=password]');
        const loginButton = $('button.button.is-primary');
        registeredemailField.setValue('apepurrem-9361@yopmail.com');
        registeredpassField.setValue('test1234');
        loginButton.click();
        assert.equal(actualUrl, "https://staging.bsa-hedonist.online/search");

>>>>>>> cb6f9025d506b16b20bc853dd00bfc73785da7e3
        browser.reloadSession();

    });

    xit('2_Should not login to Hedonist with invalid credentials', () => {
        browser.url('https://staging.bsa-hedonist.online');

        const emailField = $('input[name=email]');
        const passField = $('input[type=password]');
        const loginButton = $('button.button.is-primary');
        const errormessage = $('div.toast.is-top');
        
        emailField.setValue('ivanko.dasha@@gmail.com');
        passField.setValue('test1234');
        loginButton.click();

        errormessage.waitForDisplayed(5000);
        const errormessageText = errormessage.getText();

        assert.equal(errormessageText, "The email or password is incorrect");
        
        browser.reloadSession();
        
    });

    xit('3_Should create new place at Hedonist with valid data', () => {
        browser.url('https://staging.bsa-hedonist.online');

        const emailField = $('input[name=email]');
        const passField = $('input[type=password]');
        const loginButton = $('button.button.is-primary');
        const dropDown = $('div.profile.navbar-link');
        const link = $('a[href="/places/add"]');
        const nameFieled = $('input.input.is-medium');
        const cityField = $('div.field-body > div > div > div > div > div > div.control.is-clearfix > input');
        const zipField = $('div:nth-child(4) > div.field-body > div > div > input');
        const addressField = $('div:nth-child(5) > div.field-body > div > div > input');
        const phoneField = $('input[type=tel]');
        const websiteField = $('div:nth-child(9) > div.field-body > div > div > input');
        const descriptionField = $('.textarea');
        const loginGeneralNextbutton = $('span.button.is-success');
        const photosNextbutton = $('div:nth-child(2) > div.buttons.is-centered > span.button.is-success');
        const locationNextbutton = $('div:nth-child(3) > div.buttons.is-centered > span.button.is-success');
        const selectdropDown = $('div.tab-wrp > div:nth-child(1) > div > div > div > span > select');
        const linkcategory = $('div.tab-wrp > div:nth-child(1) > div > div > div > span > select > option:nth-child(2)');
        const tagdropDown = $('div:nth-child(2) > div > div > div > span > select');
        const taglink = $('div:nth-child(2) > div > div > div > span > select > option:nth-child(2)');
        const categoriesNextbutton = $('div:nth-child(4) > div.buttons.is-centered > span.button.is-success');
        const featuresselection = $('div > div:nth-child(1) > div.level-right > label > span.check.is-success');
        const featuresNextbutton = $('div:nth-child(5) > div.buttons.is-centered > span.button.is-success');
        const hoursNextbutton = $('div:nth-child(6) > div.buttons.is-centered > span.button.is-success');
        const confirmaddbutton = $('div:nth-child(7) > div > div.buttons.is-centered > span.button.is-success');
                    
        emailField.setValue('ivanko.dasha@gmail.com');
        passField.setValue('test123');
        loginButton.click();
        browser.pause(1000);
        dropDown.click();
        link.click();
        browser.pause(1000);
        nameFieled.setValue('My_new_place');
        cityField.setValue(null);
        cityField.setValue('Dnipro');
        zipField.setValue('49000');
        addressField.setValue('Slobozhanskiy avenue');
        phoneField.setValue('380990000000');
        websiteField.setValue('https://www.google.com');
        descriptionField.setValue('Description of new place');
        loginGeneralNextbutton.click();
        browser.pause(3000);
        const photoUploadArea=$('input[type="file"]');
        photoUploadArea.setValue("D:\\bs-academy-2019-ivanko_dasha\\image.jpg");
        browser.pause(5000);
        photosNextbutton.click();
        browser.pause(1000);
        locationNextbutton.click();
        browser.pause(1000);
        selectdropDown.click();
        linkcategory.click();
        browser.pause(1000);
        tagdropDown.click();
        taglink.click();
        categoriesNextbutton.click();
        browser.pause(1000);
        featuresselection.click();
        featuresNextbutton.click();
        browser.pause(1000);
        hoursNextbutton.click();
        browser.pause(1000);
        confirmaddbutton.click();
        browser.pause(1000);

        const title = $('div.place-venue__place-name');
        assert.equal(title.getText(),'My_new_place');

        browser.reloadSession();
       
      });

    xit('4_Should create new list at Hedonist with valid data', () => {
        browser.url('https://staging.bsa-hedonist.online');

        const emailField = $('input[name=email]');
        const passField = $('input[type=password]');
        const loginButton = $('button.button.is-primary');
        const dropDown = $('div.profile.navbar-link');
        const link = $('a[href="/my-lists"]');
        const addlistbutton = $('a.button.is-success');
        const listnameField = $('#list-name');
        const savebutton = $('button.button.is-success');
        const alert = $('div.toast.is-top');
                       
        emailField.setValue('ivanko.dasha@gmail.com');
        passField.setValue('test123');
        loginButton.click();
        browser.pause(1000);
        dropDown.click();
        link.click();
        browser.pause(1000);
        addlistbutton.click();
        browser.pause(1000);
        listnameField.setValue('My_New_List');
        savebutton.click();
              
        alert.waitForDisplayed(5000);
        const alertText = alert.getText();

        assert.equal(alertText, "The list was saved!");

        browser.reloadSession();
                    
    });

    xit('5_Should be possibility to delete list at Hedonist', () => {
        browser.url('https://staging.bsa-hedonist.online');

        const emailField = $('input[name=email]');
        const passField = $('input[type=password]');
        const loginButton = $('button.button.is-primary');
        const dropDown = $('div.profile.navbar-link');
        const link = $('a[href="/my-lists"]');
        const addlistbutton = $('a.button.is-success');
        const listnameField = $('#list-name');
        const savebutton = $('button.button.is-success');
        const deletebutton = $('div.place-item button.is-danger:enabled');
        const reconfirmbutton = $('.modal.is-active button.is-danger:enabled');
        const reply = $('div.toast.is-top');
                            
        emailField.setValue('ivanko.dasha@gmail.com');
        passField.setValue('test123');
        loginButton.click();
        browser.pause(1000);
        dropDown.click();
        link.click();
        browser.pause(1000);
        addlistbutton.click();
        browser.pause(1000);
        listnameField.setValue('My_New_List');
        savebutton.click();
        browser.pause(1000);
        deletebutton.click()
        browser.pause(5000);
        reconfirmbutton.click();
              
        reply.waitForDisplayed(5000);
        const replyText = reply.getText();

        assert.equal(replyText, "The list was removed");   
        
        browser.reloadSession();
    });
    
});