const credentials = require('../mytestdata.json');
const loginActions = require('./login_pa');
const logpageSteps = new loginActions();

const Assert = require('../ivanko_validators');

const HelpClass = require('../ivanko_helpers');
const help = new HelpClass();

const Wait = require('../ivanko_waiters');

describe('Login with valid and invalid data', () => {

    it('Should not login to Hedonist with invalid credentials', () => {
               
        help.preconditionswithoutlogin();
        logpageSteps.enterEmail(credentials.incorrectemail);
        logpageSteps.enterpassword(credentials.password);
        logpageSteps.loginconfirm();
        Wait.forNotificationToAppear();

        Assert.textofNotificationIs(credentials.invalidloginmessage);

        help.reloading();
                        
    });
 
});