const signupPage = require('./signup_po');
const signpage = new signupPage();

class signupActions {

    openSignUpPage() {
        signpage.creatnewAccountbutton.waitForDisplayed(2000);
        signpage.creatnewAccountbutton.click();
    }
    
    enterFirstName(value) {
        signpage.firstnameField.waitForDisplayed(2000);
        signpage.firstnameField.clearValue();
        signpage.firstnameField.setValue(value);
    }

    enterLastName(value) {
        signpage.lastnameField.waitForDisplayed(2000);
        signpage.lastnameField.clearValue();
        signpage.lastnameField.setValue(value);
    }

    enterEmail(value) {
        signpage.emailField.waitForDisplayed(2000);
        signpage.emailField.clearValue();
        signpage.emailField.setValue(value);
    }

    enterPassword(value) {
        signpage.newpasswordField.waitForDisplayed(2000);
        signpage.newpasswordField.clearValue();
        signpage.newpasswordField.setValue(value);
    }

    createNewUser() {
        signpage.createButton.waitForDisplayed(2000);
        signpage.createButton.click();
    }

}

module.exports = signupActions;