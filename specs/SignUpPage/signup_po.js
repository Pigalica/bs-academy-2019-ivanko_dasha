class signupPage {

    get creatnewAccountbutton () {return $('a.link.link-signup')};
    get firstnameField () {return $('input[name=firstName]')};
    get lastnameField () {return $('input[name=lastName]')};
    get emailField () {return $('input[name=email]')};
    get newpasswordField () {return $('input[type=password]')};    
    get createButton () {return $('button.button.is-primary')};
    
};

module.exports = signupPage;