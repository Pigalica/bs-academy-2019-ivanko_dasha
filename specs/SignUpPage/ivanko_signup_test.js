const credentials = require('../mytestdata.json');
const signupActions = require('./signup_pa');
const signpageSteps = new signupActions();

const assert = require('assert');
const Assert = require('../ivanko_validators');

const HelpClass = require('../ivanko_helpers');
const help = new HelpClass();

const Wait = require('../ivanko_waiters');

describe('Login with valid and invalid data', () => {
 
    it('Should create new user at Hedonist with valid data', () => {
                
        help.preconditionswithoutlogin();
        signpageSteps.openSignUpPage();
        signpageSteps.enterFirstName(credentials.firstname);
        signpageSteps.enterLastName(credentials.lastname);
        signpageSteps.enterEmail(credentials.emailfornewuser);
        signpageSteps.enterPassword(credentials.password);
        signpageSteps.createNewUser();
        Wait.forNotificationToAppear();
         
        const actualUrl = new URL(browser.getUrl());
        
        Assert.textofNotificationIs(credentials.userSuccessCreatedText);
        assert.equal(actualUrl, credentials.loginUrl);

        help.reloading();
        
    });

    it('Should not create new user at Hedonist with numbers instead characters', () => {
               
        help.preconditionswithoutlogin();
        signpageSteps.openSignUpPage();
        signpageSteps.enterFirstName(credentials.incorrectfirstname);
        signpageSteps.enterLastName(credentials.incorrectlastname);
        signpageSteps.enterEmail(credentials.newemail);
        signpageSteps.enterPassword(credentials.password);
        signpageSteps.createNewUser();
        Wait.forNotificationToAppear();

        const actualUrl = new URL(browser.getUrl());
                        
        Assert.textofNotificationIs(credentials.userisNotCreatedText);
        assert.equal(actualUrl, credentials.signupUrl);

        help.reloading();
                                 
    });
 
});