const loginActions = require('./LoginPage/login_pa');
const logpageSteps = new loginActions();

const credentials = require('./mytestdata.json');

class HelpClass 
{
    preconditionswithlogin() {
        browser.maximizeWindow();
        browser.url('https://165.227.137.250/');
        logpageSteps.enterEmail(credentials.email);
        logpageSteps.enterpassword(credentials.password);
        logpageSteps.loginconfirm();
    }

    preconditionswithoutlogin() {
        browser.maximizeWindow();
        browser.url('https://165.227.137.250/');
    }
    
    reloading() {
        browser.reloadSession();
    }

    BrowserClick(el, index) {
        return browser.execute((e, i) => {document.querySelectorAll(e)[i].click();}, el, index);
     }
   
}

module.exports = HelpClass;