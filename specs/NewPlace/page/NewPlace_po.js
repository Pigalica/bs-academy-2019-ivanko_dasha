class NewPlacePage {

    get nameFieldLable () {return $('//label[contains(., "Name*")]')};
    get nextButton () {return $('div.tab-item:not([style="display: none;"]) span.is-success')};

};

module.exports = NewPlacePage;

