class mylistsPage {

    get listnameField () {return $('#list-name')};
    get savebutton () {return $('form > div.list-name.width100 > div.form-actions > button')};
    get deletebutton () {return $('ul > li:nth-child(1) > div > div > div.place-item__actions > button')};
    get reconfirmbutton () {return $('div.animation-content.modal-content > div > div > button.button.is-danger')};
           
};

module.exports = mylistsPage;