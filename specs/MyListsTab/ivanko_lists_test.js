const credentials = require('../mytestdata.json');

const mainpageActions = require('../MainPage/mainpage_pa');
const themainpageSteps = new mainpageActions();

const listsActions = require('./mylists_pa');
const listspageSteps = new listsActions();

const Assert = require('../ivanko_validators');

const HelpClass = require('../ivanko_helpers');
const help = new HelpClass();

const Wait = require('../ivanko_waiters');

function waitForSpinner() {

    const spinner = $('div#preloader');
    spinner.waitForDisplayed(10000);
    spinner.waitForDisplayed(10000, true);
};

describe('Creation and deletion of lists', () => {
             
    it('Should create new list at Hedonist with valid data', () => {
        
        help.preconditionswithlogin();
        browser.pause(1000);
        themainpageSteps.openDropDown();
        help.BrowserClick('a[href="/my-lists"]', 0);
        browser.pause(1000);
        help.BrowserClick('div.add-list.has-text-right > a', 0);
        listspageSteps.enternameofnewlist(credentials.newlistname);
        listspageSteps.savenewlistbutton();
        Wait.forNotificationToAppear();
                
        Assert.textofNotificationIs(credentials.newlistSuccessCreatedText);

        help.reloading();
                    
    });
     
    it('Should be possibility to delete list at Hedonist', () => {
        
        help.preconditionswithlogin();
        browser.pause(1000);
        themainpageSteps.openDropDown();
        help.BrowserClick('a[href="/my-lists"]', 0);
        waitForSpinner();
        help.BrowserClick('div.add-list.has-text-right > a', 0);
        listspageSteps.enternameofnewlist(credentials.newlistname);
        listspageSteps.savenewlistbutton();
        listspageSteps.deletenewlistbutton();
        browser.pause(2000);
        listspageSteps.confirmtodeletenewlistbutton();
        Wait.forNotificationToAppear();

        Assert.textofNotificationIs(credentials.newlistSuccessDeletedText);

        help.reloading();
               
    });
    
});