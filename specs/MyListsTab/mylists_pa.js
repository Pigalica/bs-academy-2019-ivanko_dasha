const mylistsPage = require('./mylists_po');
const listspage = new mylistsPage();

class listsActions {

    enternameofnewlist(value) {
        listspage.listnameField.waitForExist(5000);
        listspage.listnameField.clearValue();
        listspage.listnameField.setValue(value);
    }

    savenewlistbutton() {
        listspage.savebutton.waitForExist(5000);
        listspage.savebutton.click();
    }

    deletenewlistbutton() {
        listspage.deletebutton.waitForExist(5000);
        listspage.deletebutton.click();
    }

    confirmtodeletenewlistbutton() {
        listspage.reconfirmbutton.waitForExist(5000);
        listspage.reconfirmbutton.click();
    }

}

module.exports = listsActions;