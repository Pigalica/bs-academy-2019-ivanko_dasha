const newplacePage = require('./newplace_po');
const placepage = new newplacePage();

class placeActions {

    enterName(value) {
        placepage.nameFieled.waitForDisplayed(5000);
        placepage.nameFieled.clearValue();
        placepage.nameFieled.setValue(value);
    }

    enterCity(value) {
        placepage.cityField.waitForDisplayed(2000);
        placepage.cityField.clearValue();
        placepage.cityField.setValue(value);
    }

    enterZip(value) {
        placepage.zipField.waitForDisplayed(2000);
        placepage.zipField.clearValue();
        placepage.zipField.setValue(value);
    }

    enterAddress(value) {
        placepage.addressField.waitForDisplayed(2000);
        placepage.addressField.clearValue();
        placepage.addressField.setValue(value);
    }

    enterPhone(value) {
        placepage.phoneField.waitForDisplayed(2000);
        placepage.phoneField.clearValue();
        placepage.phoneField.setValue(value);
    }

    enterWebsite(value) {
        placepage.websiteField.waitForDisplayed(2000);
        placepage.websiteField.clearValue();
        placepage.websiteField.setValue(value);
    }

    enterDescription(value) {
        placepage.descriptionField.waitForDisplayed(2000);
        placepage.descriptionField.clearValue();
        placepage.descriptionField.setValue(value);
    }

    nextbutton() {
        placepage.loginGeneralNextbutton.waitForDisplayed(2000);
        placepage.loginGeneralNextbutton.click();
    }

    uploadphoto(path) {
        placepage.photoUploadArea.waitForExist(2000);
        placepage.photoUploadArea.setValue(path);
    }

    next2button() {
        placepage.photosNextbutton.waitForDisplayed(2000);
        placepage.photosNextbutton.click();
    }

    next3button() {
        placepage.locationNextbutton.waitForDisplayed(2000);
        placepage.locationNextbutton.click();
    }

    categoryDropdown() {
        placepage.selectdropDown.waitForDisplayed(2000);
        placepage.selectdropDown.click();
    }

    selectcategory() {
        placepage.linkcategory.waitForDisplayed(2000);
        placepage.linkcategory.click();
    }

    tagDropdown() {
        placepage.tagdropDown.waitForDisplayed(2000);
        placepage.tagdropDown.click();
    }

    selecttag() {
        placepage.taglink.waitForDisplayed(2000);
        placepage.taglink.click();
    }

    next4button() {
        placepage.categoriesNextbutton.waitForDisplayed(2000);
        placepage.categoriesNextbutton.click();
    }

    selectfeature() {
        placepage.featuresselection.waitForDisplayed(2000);
        placepage.featuresselection.click();
    }

    next5button() {
        placepage.featuresNextbutton.waitForDisplayed(2000);
        placepage.featuresNextbutton.click();
    }
  
    confirmplacecreation() {
        placepage.confirmaddbutton.waitForDisplayed(5000);
        placepage.confirmaddbutton.waitForEnabled(5000);
        placepage.confirmaddbutton.click();
    }

}

module.exports = placeActions;