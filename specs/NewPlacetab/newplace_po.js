class newplacePage {

    get nameFieled () {return $('input.input.is-medium')};
    get cityField () {return $('div.field-body > div > div > div > div > div > div.control.is-clearfix > input')};
    get zipField () {return $('div:nth-child(4) > div.field-body > div > div > input')};
    get addressField () {return $('div:nth-child(5) > div.field-body > div > div > input')};
    get phoneField () {return $('input[type=tel]')};
    get websiteField () {return $('div:nth-child(9) > div.field-body > div > div > input')};
    get descriptionField () {return $('.textarea')};
    get loginGeneralNextbutton () {return $('span.button.is-success')};
    get photoUploadArea () {return $('input[type="file"]')};
    get photosNextbutton () {return $('div:nth-child(2) > div.buttons.is-centered > span.button.is-success')};
    get locationNextbutton () {return $('div:nth-child(3) > div.buttons.is-centered > span.button.is-success')};
    get selectdropDown () {return $('div.tab-wrp > div:nth-child(1) > div > div > div > span > select')};
    get linkcategory () {return $('div.tab-wrp > div:nth-child(1) > div > div > div > span > select > option:nth-child(2)')};
    get tagdropDown () {return $('div:nth-child(2) > div > div > div > span > select')};
    get taglink () {return $('div:nth-child(2) > div > div > div > span > select > option:nth-child(2)')};
    get categoriesNextbutton () {return $('div:nth-child(4) > div.buttons.is-centered > span.button.is-success')};
    get featuresselection () {return $('div > div:nth-child(1) > div.level-right > label > span.check.is-success')};
    get featuresNextbutton () {return $('div:nth-child(5) > div.buttons.is-centered > span.button.is-success')};
    get confirmaddbutton () {return $('div:nth-child(7) > div > div.buttons.is-centered > span.button.is-success')};
          
};

module.exports = newplacePage;