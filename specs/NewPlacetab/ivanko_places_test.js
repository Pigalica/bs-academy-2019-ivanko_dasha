const assert = require('assert');
const credentials = require('../mytestdata.json');

const mainpageActions = require('../MainPage/mainpage_pa');
const themainpageSteps = new mainpageActions();

const placeActions = require('./newplace_pa');
const placepageSteps = new placeActions();

const HelpClass = require('../ivanko_helpers');
const help = new HelpClass();

describe('Creation of new place', () => {
    
    it('Should create new place at Hedonist with valid data', () => {
                
        help.preconditionswithlogin();
        themainpageSteps.openDropDown();
        help.BrowserClick('a[href="/places/add"]', 0);
        placepageSteps.enterName(credentials.nameofplace);
        placepageSteps.enterCity(credentials.cityofplace);
        placepageSteps.enterZip(credentials.zipofplace);
        placepageSteps.enterAddress(credentials.addressofplace);
        placepageSteps.enterPhone(credentials.phoneofplace);
        placepageSteps.enterWebsite(credentials.websiteofplace);
        placepageSteps.enterDescription(credentials.descriptionofplace);
        placepageSteps.nextbutton();
        placepageSteps.uploadphoto(credentials.pathtoimage);
        placepageSteps.next2button();
        placepageSteps.next3button();
        placepageSteps.categoryDropdown();
        placepageSteps.selectcategory();
        placepageSteps.tagDropdown();
        placepageSteps.selecttag();
        placepageSteps.next4button();
        placepageSteps.selectfeature();    
        placepageSteps.next5button();
        help.BrowserClick('div:nth-child(6) > div.buttons.is-centered > span.button.is-success', 0);
        placepageSteps.confirmplacecreation();
              
        const title = $('div.place-venue__place-name');
        assert.equal(title.getText(), credentials.nameofplace);

        help.reloading();
             
      });
   
});